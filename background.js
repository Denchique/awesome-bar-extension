chrome.browserAction.onClicked.addListener(function() {

    chrome.tabs.getAllInWindow(function(tab) {
        var all_tabs = [];
        for (var i = 0; i < tab.length; i++) {
            all_tabs.push({
                "id": tab[i].id,
                "url": tab[i].url,
                "title": tab[i].title
            });
        };

        // insert tabs to local storage
        chrome.storage.sync.set({
            'all_tabs': all_tabs,
        });

        chrome.tabs.create({
            'url': "./onetab.html"
        });

        // close all tabs
        for (var i = 0; i < all_tabs.length; i++) {
            chrome.tabs.remove(all_tabs[i]['id']);
        }

    });

});