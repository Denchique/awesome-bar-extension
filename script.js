// get favico https://www.google.com/s2/favicons?domain_url=

function newTabItem(id, title, url) {

    t = document.createElement("div");
    t.className = "tab_title";
    t.innerHTML = "[" + id + "] " + title.substring(0, 100) + " ...";

    u = document.createElement("a");
    u.className = "tab_url";
    u.innerHTML = url.substring(0, 50) + " ..."
    u.setAttribute("href", url);

    fav = document.createElement("img");
    fav.className = "favico";
    fav.setAttribute("src", "https://www.google.com/s2/favicons?domain_url=" + url);


    wrapper.appendChild(t);
    wrapper.appendChild(fav);
    wrapper.appendChild(u);
    wrapper.appendChild(document.createElement("hr"));
}


chrome.storage.sync.get(['all_tabs'], function(items) {
    tabs = items['all_tabs'];
    var wrapper = document.getElementById("wrapper");
    for (var i = 0; i != tabs.length; i++) {
        newTabItem(tabs[i]['id'], tabs[i]['title'], tabs[i]['url'])
    }


    // show total
    document.getElementsByClassName("total_tabs")[0].innerHTML = tabs.length;
});